const express = require('express');
const router = express.Router();

const fire = require('../fire');

/* GET responses listing. */
router.get('/', function(req, res, next) {
  res.send('Please browse responses by id: /response/:responseId. Later on maybe we will put here a list of all resposnse.');
});


/* GET response listing. */
router.get('/:responseId', async function(req, res, next) {

  const responseId = req.params.responseId;

  try {
    const response = await fire.getResponseById(responseId);

    if(!response.val()) {
      throw(`Response ${responseId} doesn't exist`);
    }

    if(!response.val().body) {
      throw(`Response ${responseId} doesn't have body!`);
    }

    res.status(200).json(response.val().body);

  } catch(error) {
    console.log(error);
    res.send(error);
  }

});

module.exports = router;
