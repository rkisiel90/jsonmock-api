const express = require('express');
const router = express.Router();

const fire = require('../fire');




/* GET mock listing. */
router.get('/', function(req, res, next) {
  res.send('Please browse mocks by id: /mocks/:mockId. Later on maybe we will put here a list of all mocks.');
});





function bumpupStep(mockId) {
  fire.dbRef.ref().child(`mocks/${mockId}`).transaction(function(mock) {
    if (mock) {
      // REMEMBER: mock.step is 0 based!

      if(mock.hasOwnProperty('step')) {
        return {...mock, step: (mock.step === mock.responses.length - 1) ? 0 : mock.step + 1};
      } else if(mock.responses && mock.responses.length > 1) {
        return {...mock, step: 0};
      }
    }
    return mock;
  });
}

/* GET mock listing. */
router.get('/:mockId', async function(req, res, next) {

  const mockId = req.params.mockId;

  try {
    const start = new Date();
    const mock = await fire.getMockById(mockId);

    if(!mock.val()) {
      throw(`Mock with ${mockId} id doesn't exist`);
    }

    const currentMockStep = (mock.val().step && mock.val().step > 0) ? mock.val().step : 0;

    const response = await fire.getResponseById(mock.val().responses[currentMockStep]);

    if(!response.val()) {
      throw(`Response ${mock.val().responses[0]} doesn't exist`);
    }

    if(!response.val().body) {
      bumpupStep(mockId);
      throw(`Response ${mock.val().responses[0]} doesn't have body!`);
    }

    const end = new Date();

    const readingTime = end - start;
    let timeLeft = 0;

    if(mock.val().gap) {
      console.log('Gap is ', mock.val().gap);
      console.log('Getting response took ', readingTime);
      timeLeft = (mock.val().gap - readingTime > 0) ? mock.val().gap - readingTime : 0;

      console.log('We will delay response about', timeLeft);

    }
    const hangTime = (timeLeft > 0) ? timeLeft : 0;

    setTimeout(function(){
      res.status(200).json(response.val().body);

      bumpupStep(mockId);
    }, hangTime);

  } catch(error) {
    res.send(error);
  }



});

module.exports = router;
