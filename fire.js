const firebase = require('firebase');

const config = {
	apiKey: "AIzaSyCWxBvx1MvUhCwan8dEcvMSjntWW_0jbYk",
	authDomain: "jsonmock-cb2ed.firebaseapp.com",
	databaseURL: "https://jsonmock-cb2ed.firebaseio.com",
	projectId: "jsonmock-cb2ed",
	storageBucket: "jsonmock-cb2ed.appspot.com",
	messagingSenderId: "437674086167"
};

const fire = firebase.initializeApp(config);
const dbRef = fire.database();


async function getResponseById(responseId) {
	return dbRef.ref('/responses/').child(responseId).once('value');
}

async function getMockById(mockId) {
	return dbRef.ref('/mocks/').child(mockId).once('value');
}

module.exports = fire;
module.exports.dbRef = dbRef;
module.exports.getResponseById = getResponseById;
module.exports.getMockById = getMockById;